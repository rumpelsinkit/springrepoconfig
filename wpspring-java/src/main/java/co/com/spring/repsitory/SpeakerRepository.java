package co.com.spring.repsitory;

import co.com.spring.model.Speaker;

import java.util.List;

public interface SpeakerRepository {
    List<Speaker> findAll();
}
