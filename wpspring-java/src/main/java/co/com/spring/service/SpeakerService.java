package co.com.spring.service;

import co.com.spring.model.Speaker;

import java.util.List;

public interface SpeakerService {
    List<Speaker> findAll();
}
