package co.com.spring.service;

import co.com.spring.model.Speaker;
import co.com.spring.repsitory.HibernateSpeakerRepositoryImpl;
import co.com.spring.repsitory.SpeakerRepository;

import java.util.List;

public class SpeakerServiceImpl implements SpeakerService {

    private SpeakerRepository repository = new HibernateSpeakerRepositoryImpl();

    public List<Speaker> findAll(){
        return repository.findAll();
    }
}
